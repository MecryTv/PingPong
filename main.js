/*==================== Loading Circle ====================*/
document.addEventListener("DOMContentLoaded", function () {
  setTimeout(function () {
    document.getElementById("loading-screen").style.display = "none";

    document.getElementById("content").classList.remove("hidden");
  }, 2000);
});

/*==================== Welcome Screen ====================*/
setTimeout(() => {
  document.getElementById("welcomeScreen").classList.add("hide");

  setTimeout(() => {
    document.getElementById("welcomeScreen").style.display = "none";
    document.getElementById("startScreen").classList.add("show");
  }, 1000);
}, 5000);

/*==================== Game Logic ====================*/
let ctx = ca.getContext("2d");
let p1 = 80;
let p2 = 200;
let key = {};
let bal = {
  x: 360,
  y: 240,
  speedX: 0,
  speedY: 0,
};
let score1 = 0;
let score2 = 0;
let gameActive = false;
let player1Name = "";
let player2Name = "";
let pointsToWin = 0;
let countdown = 5;
let countdownInterval;

document.addEventListener("keydown", (e) => (key[e.keyCode] = true));
document.addEventListener("keyup", (e) => (key[e.keyCode] = false));
document.getElementById("startButton").addEventListener("click", startGame);

function startGame() {
  player1Name = document.getElementById("player1Name").value.trim();
  player2Name = document.getElementById("player2Name").value.trim();
  pointsToWin = parseInt(document.getElementById("pointsToWin").value, 10);

  if (!player1Name || !player2Name || !pointsToWin) {
    alert("Please fill in all fields and enter a valid game length");
    return;
  }

  if (pointsToWin <= 0) {
    alert("The play length must be greater than 0");
    return;
  }

  if (player1Name === player2Name) {
    alert("The names of the players must not be identical");
    return;
  }

  if (player1Name.length < 4 || player2Name.length < 4) {
    alert("The names of the players must be at least 4 characters long");
    return;
  }

  document.getElementById("startScreen").style.display = "none";
  document.getElementById("gameContainer").style.display = "block";
  document.getElementById("player1").textContent = player1Name;
  document.getElementById("player2").textContent = player2Name;
  updateScore();

  document.getElementById(
    "scoreboard"
  ).textContent = `Game starts in ${countdown} Seconds`;
  countdownInterval = setInterval(() => {
    countdown--;
    if (countdown <= 0) {
      clearInterval(countdownInterval);
      gameActive = true;
      draw();
      startBall();
      updateScore();
    } else {
      document.getElementById(
        "scoreboard"
      ).textContent = `Game starts in ${countdown} Seconds`;
    }
  }, 1000);
}

function clearCanvas() {
  ctx.clearRect(0, 0, ca.width, ca.height);
}

function draw() {
  clearCanvas();
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, 720, 480);
  ctx.fillStyle = "white";
  ctx.fillRect(10, p1, 10, 80);
  ctx.fillRect(700, p2, 10, 80);
  ctx.fillRect(bal.x, bal.y, 10, 10);
  requestAnimationFrame(draw);
}

function loop() {
  if (!gameActive) return;

  if (key[38] && p2 > 0) {
    p2 = p2 - 5;
  } else if (key[40] && p2 < 400) {
    p2 = p2 + 5;
  } else if (key[87] && p1 > 0) {
    p1 = p1 - 5;
  } else if (key[83] && p1 < 400) {
    p1 = p1 + 5;
  }
  bal.x = bal.x + bal.speedX;
  bal.y = bal.y + bal.speedY;

  if (bal.x < 20 || bal.x > 690) {
    if (bal.y > p1 && bal.y < p1 + 80 && bal.speedX < 0) {
      bal.speedX = -bal.speedX;
      bal.speedY = (bal.y - p1 - 40) * 0.1;
    }

    if (bal.y > p2 && bal.y < p2 + 80 && bal.speedX > 0) {
      bal.speedX = -bal.speedX;
      bal.speedY = (bal.y - p2 - 40) * 0.1;
    }
  }

  if (bal.x < 0) {
    score2++;
    resetBall();
    checkWinner();
  } else if (bal.x > 720) {
    score1++;
    resetBall();
    checkWinner();
  }

  if (bal.y < 0 || bal.y > 480) {
    bal.speedY = -bal.speedY;
  }

  updateScore();
}

function startBall() {
  let angle = Math.random() * Math.PI * 2;
  bal.speedX = 3 * Math.cos(angle);
  bal.speedY = 3 * Math.sin(angle);
}

function resetBall() {
  bal.x = 360;
  bal.y = 240;
  bal.speedX = 0;
  bal.speedY = 0;
  if (gameActive) {
    setTimeout(startBall, 1000);
  }
}

function updateScore() {
  document.getElementById("scoreboard").textContent = `${score1} | ${score2}`;
}

function checkWinner() {
  if (score1 >= pointsToWin || score2 >= pointsToWin) {
    gameActive = false;
    let winnerName = score1 >= pointsToWin ? player1Name : player2Name;
    let winnerPoints = score1 >= pointsToWin ? score1 : score2;
    document.getElementById("gameContainer").style.display = "none";
    document.getElementById(
      "winner"
    ).textContent = `${winnerName} won the game by ${winnerPoints} points!`;
    document.getElementById("winner").style.display = "block";

    setTimeout(() => {
      document.getElementById("gameContainer").style.display = "none";
      document.getElementById("winner").style.display = "none";
      document.getElementById("startScreen").style.display = "flex";
      countdown = 5;
      clearInterval(countdownInterval);
      document.getElementById("scoreboard").textContent = "";
      score1 = 0;
      score2 = 0;
      updateScore();

      document.getElementById("player1Name").value = "";
      document.getElementById("player2Name").value = "";
      document.getElementById("pointsToWin").value = "";
    }, 5000);
  }
}

setInterval(loop, 1000 / 60);
